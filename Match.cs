﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data.SqlClient;

namespace DBMS_Backend
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class UserFilterCriteria
    {
        public string College { get; set; }
        public string Department { get; set; }
        public string Sex { get; set; }
        public string City { get; set; }
        public List<string> Language { get; set; }
        public List<string> Personality { get; set; }
        public List<string> Interest { get; set; }

        // Constructor
        public UserFilterCriteria()
        {
            Personality = new List<string>();
            Language = new List<string>();
            Interest = new List<string>();
        }
    }
    //Show
    public class PairUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string College { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public int Age { get; set; }
        public List<string> Language { get; set; }
        public List<string> Personality { get; set; }
        public List<string> Interest { get; set; }
    }
    //Pairing
    public class PairingRequest
    {
        public int User1Id { get; set; }
        public int User2Id { get; set; }
    }

}

