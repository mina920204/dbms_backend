﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace DBMS_Backend
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class UserFilterCriteria
    {
        public string? College { get; set; }
        public string? Department { get; set; }
        public string? Sex { get; set; }
        public string? City { get; set; }
        public List<string>? Language { get; set; }
        public List<string>? Personality { get; set; }
        public List<string>? Interest { get; set; }
    }

    //Show
    public class PairUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string College { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public int Age { get; set; }
        public string Path { get; set; }
        public string Avatar {get; set;}
        public List<string> Language { get; set; }
        public List<string> Personality { get; set; }
        public List<string> Interest { get; set; }
    }
    //Pairing
    public class PairingRequest
    {
        public int User1Id { get; set; }
        public int User2Id { get; set; }
    }

}

namespace DBMS_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        //private string connectionString = "Server=DESKTOP-61HJH7T;Database=Message_in_bottle;Integrated Security=True;"; // Replace with your SQL Server connection string
        private readonly IConfiguration _configuration;

        public UsersController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        [HttpGet("random")]
        public async Task<IActionResult> GetRandomUser()
        {
            try
            {
                User randomUser = await GenerateRandomUserAsync();
                return Ok(randomUser);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "An error occurred while fetching a random user from the database: " + ex.Message);
            }
        }

        [HttpPost("filter")]
        public async Task<IActionResult> GetUsersByFilter([FromBody] UserFilterCriteria criteria)
        {
       
            try
            {
                List<User> filteredUsers = await GetUsersByFilterAsync(criteria);
                return Ok(filteredUsers);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "An error occurred while fetching users by filter criteria: " + ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserById(int id)
        {
            
            try
            {
                User user = await GetUserByIdAsync(id);
                if (user != null)
                {
                    return Ok(user);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "An error occurred while fetching the user from the database: " + ex.Message);
            }
        }

        //show user
        [HttpGet]
        [Route("ShowUserById/{userId}")]
        public async Task<IActionResult> ShowUserById(int userId)
        {
            try
            {
                // Call the function to fetch user information by ID
                PairUser user = await ShowUserByIdAsync(userId);

                if (user == null)
                {
                    return NotFound($"User with ID {userId} not found.");
                }

             
             
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }



        [HttpPost]
        [Route("InsertUserPair")]
        public async Task<IActionResult> InsertUserPairAsync(int user1Id, int user2Id)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    
                    // Build the SQL query to insert the user pair
                    string insertQuery = @"INSERT INTO MATCH (Matcher_id, MatchedAcc_id) VALUES (@user1Id, @user2Id); SELECT SCOPE_IDENTITY()";

                    using (SqlCommand command = new SqlCommand(insertQuery, connection))
                    {
                    // Set the parameters for the user IDs
                    command.Parameters.AddWithValue("@user1Id", user1Id);
                    command.Parameters.AddWithValue("@user2Id", user2Id);

                    int pairId = Convert.ToInt32(await command.ExecuteScalarAsync());
                    // Use the pairId as needed
                    // Execute the SQL command
                    await command.ExecuteNonQueryAsync();

                    Console.WriteLine("User pair inserted successfully.");
                    }
                }
                return Ok("User pair created successfully.");
            }
            catch (Exception ex)
            {
                // Log the exception or handle it appropriately
                Console.WriteLine(ex.ToString());

                // Return an error response with a specific message
                return StatusCode(500, "An error occurred while inserting pair");
            }
        }
               
        private async Task<User> GenerateRandomUserAsync()
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))

                {
                    await connection.OpenAsync();

                    // NEWID() function is to generate random rows from a table to fetch a random user
                    using (SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM ACCOUNT ORDER BY NEWID()", connection))
                    {
                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                User randomUser = new User
                                {
                                    Id = reader.GetInt32(reader.GetOrdinal("Acc_id")),
                                    Name = reader.GetString(reader.GetOrdinal("Name"))
                                };

                                return randomUser;
                            }
                            else
                            {
                                Console.WriteLine("No users found in the database.");
                                return null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while fetching a random user from the database:");
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private async Task<List<User>> GetUsersByFilterAsync([FromBody] UserFilterCriteria criteria)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    // Extract the filter criteria from the UserFilterCriteria object
                    string college = criteria.College;
                    string dep = criteria.Department;
                    string sex = criteria.Sex;
                    string city = criteria.City;
                    List<string> language = criteria.Language;
                    List<string> interest = criteria.Interest;
                    List<string> personality = criteria.Personality;

                    // Convert lists to comma-separated strings or set to NULL if empty
                    string languageString = language.Any() ? string.Join(",", language) : "NULL";
                    string interestString = interest.Any() ? string.Join(",", interest) : "NULL";
                    string personalityString = personality.Any() ? string.Join(",", personality) : "NULL";

                    // Build the SQL query to retrieve the most similar users
                    string query = $@"SELECT DISTINCT TOP 2 A.*, 
                    (CASE WHEN A.College = @college THEN 1 ELSE 0 END) +
                    (CASE WHEN A.Dep = @dep THEN 1 ELSE 0 END) +
                    (CASE WHEN A.Sex = @sex THEN 1 ELSE 0 END) +
                    (CASE WHEN A.City = @city THEN 1 ELSE 0 END) +
                    (CASE WHEN C.C = 1 AND (C.Content IN ({languageString}) OR {languageString} IS NULL) THEN 1 ELSE 0 END) +
                    (CASE WHEN C.C = 2 AND (C.Content IN ({interestString}) OR {interestString} IS NULL) THEN 1 ELSE 0 END) +
                    (CASE WHEN C.C = 3 AND (C.Content IN ({personalityString}) OR {personalityString} IS NULL) THEN 1 ELSE 0 END) AS SimilarityScore 
                    FROM account A
                    JOIN category C ON A.Acc_id = C.Acc_id
                    WHERE (@college IS NULL OR A.College = @college)
                      AND (@dep IS NULL OR A.Dep = @dep)
                      AND (@sex IS NULL OR A.Sex = @sex)
                      AND (@city IS NULL OR A.City = @city)
                      AND ((C.C = 1 AND (C.Content IN ({languageString}) OR {languageString} IS NULL))
                            OR (C.C = 2 AND (C.Content IN ({interestString}) OR {interestString} IS NULL))
                            OR (C.C = 3 AND (C.Content IN ({personalityString}) OR {personalityString} IS NULL)))
                       ORDER BY SimilarityScore DESC";



                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        // Set the parameters for the filter criteria
                        command.Parameters.AddWithValue("@college", criteria.College ?? (object)DBNull.Value);
                        command.Parameters.AddWithValue("@dep", criteria.Department ?? (object)DBNull.Value);
                        command.Parameters.AddWithValue("@sex", criteria.Sex ?? (object)DBNull.Value);
                        command.Parameters.AddWithValue("@city", criteria.City ?? (object)DBNull.Value);

                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            List<User> users = new List<User>();

                            while (await reader.ReadAsync())
                            {
                                User user = new User
                                {
                                    Id = reader.GetInt32(reader.GetOrdinal("Acc_id")),
                                    Name = reader.GetString(reader.GetOrdinal("Name")),
                                };

                                users.Add(user);
                            }

                            return users;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while fetching the user from the database:");
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private async Task<User> GetUserByIdAsync(int id)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM ACCOUNT WHERE Acc_id = @id", connection))
                    {
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                User user = new User
                                {
                                    Id = reader.GetInt32(reader.GetOrdinal("Acc_id")),
                                    Name = reader.GetString(reader.GetOrdinal("Name")),
                                };
                                return user;
                            }
                            else
                            {
                                Console.WriteLine("User not found in the database.");
                                return null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while fetching the user from the database:");
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        //Show user by id
        private async Task<PairUser> ShowUserByIdAsync(int userId)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    // Build the SQL query to retrieve user information by ID
                    string query = @"SELECT A.Name, A.College, A.Sex, A.City, A.Age, A.Avatar, C1.Content AS Language, C2.Content AS Interest, C3.Content AS Personality
                             FROM account A
                             LEFT JOIN (
                                SELECT Acc_id, STRING_AGG(Content, ',') AS Content
                                FROM category C
                                WHERE C = 1
                                GROUP BY Acc_id
                             ) C1 ON A.Acc_id = C1.Acc_id
                             LEFT JOIN (
                                SELECT Acc_id, STRING_AGG(Content, ',') AS Content
                                FROM category C
                                WHERE C = 2
                                GROUP BY Acc_id
                             ) C2 ON A.Acc_id = C2.Acc_id
                             LEFT JOIN (
                                SELECT Acc_id, STRING_AGG(Content, ',') AS Content
                                FROM category C
                                WHERE C = 3
                                GROUP BY Acc_id
                             ) C3 ON A.Acc_id = C3.Acc_id
                             WHERE A.Acc_id = @userId";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        // Set the parameter for the user ID
                        command.Parameters.AddWithValue("@userId", userId);

                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                // Create a User object and populate its properties from the database
                                PairUser user = new PairUser
                                {
                                    Id = userId,
                                    Name = reader.GetString(reader.GetOrdinal("Name")),
                                    College = reader.GetString(reader.GetOrdinal("College")),
                                    Gender = reader.GetString(reader.GetOrdinal("Sex")),
                                    City = reader.GetString(reader.GetOrdinal("City")),
                                    Age = reader.GetInt32(reader.GetOrdinal("Age")),
                                    Path = reader.GetString(reader.GetOrdinal("Avatar")),
                                    Language = reader.GetString(reader.GetOrdinal("Language")).Split(',').ToList(),
                                    Interest = reader.GetString(reader.GetOrdinal("Interest")).Split(',').ToList(),
                                    Personality = reader.GetString(reader.GetOrdinal("Personality")).Split(',').ToList()
                                };

                                // Read the picture data from the file
                                byte[] pictureData = System.IO.File.ReadAllBytes(user.Path);

                                // Convert the picture data to a Base64 string
                                string base64Image = Convert.ToBase64String(pictureData);

                                // Set the Base64 string as the Avatar property
                                user.Avatar = base64Image;

                                return user;
                            }
                        }
                    }
                }

                return null; // User not found
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while fetching user information from the database:");
                Console.WriteLine(ex.Message);
                return null;
            }
        }
     }
}
