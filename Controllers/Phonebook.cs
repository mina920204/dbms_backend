﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.Metrics;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileSystemGlobbing;

namespace DBMS_Backend
{
    public class MatchedPair
    {
        public int MatchId { get; set; }
        public int MatcherId { get; set; }
        public int MatchedAccId { get; set; }
        public string Note { get; set; }
    }

}

namespace DBMS_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhonebookController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public PhonebookController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("MatchedPairs")]
        public async Task<IActionResult> GetMatchedPairs(int userId)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    string query = @"
                            SELECT MatchedAcc_id
                            FROM MATCH
                            WHERE Matcher_id = @UserId
                            AND MatchedAcc_id IN (
                                SELECT Matcher_id
                                FROM MATCH
                                WHERE MatchedAcc_id = @UserId
                            )
                            GROUP BY MatchedAcc_id";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);

                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            List<int> matchedAccIds = new List<int>();

                            while (reader.Read())
                            {
                                int matchedAccId = reader.GetInt32(0);
                                matchedAccIds.Add(matchedAccId);
                            }

                            return Ok(matchedAccIds);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }


        [HttpGet]
        [Route("Pending/{userId}")]
        public async Task<IActionResult> GetPending(int userId)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    // Query to retrieve the pending pairs for the specified user ID
                    string query = @"
                                    SELECT DISTINCT Matcher_id
                                    FROM MATCH
                                    WHERE MatchedAcc_id = @UserId
                                    AND Matcher_id NOT IN (
                                        SELECT MatchedAcc_id
                                        FROM MATCH
                                        WHERE Matcher_id = @UserId
                                    )";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);

                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            List<int> pendingAccountIds = new List<int>();

                            while (reader.Read())
                            {
                                int accountId = reader.GetInt32(0);
                                if (!pendingAccountIds.Contains(accountId))
                                {
                                    pendingAccountIds.Add(accountId);
                                }
                            }

                            return Ok(pendingAccountIds);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
    }
}