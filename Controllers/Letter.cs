﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.Metrics;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace DBMS_Backend
{
    public class Letter
    {
        public int LetterId { get; set; }
        public int MatchId { get; set; }
        public string Topic { get; set; }
        public string Content { get; set; }
        public DateTime Time { get; set; }
        public IFormFile? Att { get; set; }
        public string? AttType { get; set; }
        public int MatcherId { get; set; }
        public int MatchedAccountId { get; set; }
    }
}

namespace DBMS_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LetterController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public LetterController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost("SendLetter")]
        public async Task<IActionResult> SendLetter([FromBody] Letter letter)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");

            // Extract the properties from the letter object
            int MatcherId = letter.MatcherId;
            int MatchedAccId = letter.MatchedAccountId;
            string topic = letter.Topic;
            string content = letter.Content;
            IFormFile? att = letter.Att;
            string? attType = letter.AttType;
            DateTime time = letter.Time;

            try
            {
                // Check if the required fields are provided
                if (string.IsNullOrEmpty(topic) || string.IsNullOrEmpty(content))
                {
                    return BadRequest("The 'topic' and 'content' fields are required.");
                }

                // Check if an attachment file is provided
                byte[]? attBytes = null;
                if (att != null && att.Length > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await att.CopyToAsync(memoryStream);
                        attBytes = memoryStream.ToArray();
                    }
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    // Insert the letter into the Letters table
                    string insertQuery = "INSERT INTO LETTER (Match_id, Topic, Content, Time, Att, AttType) " +
                                         "VALUES ((SELECT Match_id FROM MATCH WHERE Matcher_id = @MatcherId AND MatchedAcc_id = @MatchedAccId), @Topic, @Content, @Time, @Att, @AttType)" +
                                         "SELECT SCOPE_IDENTITY() AS LetterId;";

                    using (SqlCommand insertCommand = new SqlCommand(insertQuery, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@MatcherId", MatcherId);
                        insertCommand.Parameters.AddWithValue("@MatchedAccId", MatchedAccId);
                        insertCommand.Parameters.AddWithValue("@Topic", topic);
                        insertCommand.Parameters.AddWithValue("@Content", content);
                        insertCommand.Parameters.AddWithValue("@Time", time);
                        insertCommand.Parameters.AddWithValue("@Att", (object)attBytes ?? DBNull.Value);
                        //insertCommand.Parameters.AddWithValue("@Att", attBytes != null ? (object)attBytes : DBNull.Value);
                        insertCommand.Parameters.AddWithValue("@AttType", !string.IsNullOrEmpty(attType) ? (object)attType : DBNull.Value);

                        // Execute the insert command and get the generated LetterId
                        int letterId = Convert.ToInt32(await insertCommand.ExecuteScalarAsync());

                        return Ok($"Letter sent successfully. LetterId: {letterId}");
                    }
                }
            }
            catch (Exception ex)
            {
                // Log the exception or handle it appropriately
                Console.WriteLine(ex.ToString());

                // Return an error response with a specific message
                return StatusCode(500, "An error occurred while sending the letter.");
            }
        }


        //Get all letters from the specific user
        [HttpGet]
        public async Task<IActionResult> GetReceivedLetters(int matchedAccountId, int matcherId)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");

            try
            {
                List<Letter> receivedLetters = new List<Letter>();

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    string query = "SELECT L.Le_id, L.Match_id, L.Topic, L.Content, L.Time, L.Att, L.AttType, " +
                                   "M.Matcher_id, M.MatchedAcc_id " +
                                   "FROM LETTER L " +
                                   "JOIN MATCH M ON L.Match_id = M.Match_id " +
                                   "WHERE M.MatchedAcc_id = @MatchedAccountId AND M.Matcher_id = @MatcherId " +
                                   "ORDER BY L.Time DESC";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@MatchedAccountId", matchedAccountId);
                        command.Parameters.AddWithValue("@MatcherId", matcherId);

                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Letter letter = new Letter
                                {
                                    LetterId = Convert.ToInt32(reader["Le_id"]),
                                    MatchId = Convert.ToInt32(reader["Match_id"]),
                                    Topic = reader["Topic"].ToString(),
                                    Content = reader["Content"].ToString(),
                                    Time = Convert.ToDateTime(reader["Time"]),
                                    //Att = reader["Att"] != DBNull.Value ? (byte[])reader["Att"] : null,
                                    AttType = reader["AttType"] != DBNull.Value ? reader["AttType"].ToString() : null,
                                    MatcherId = Convert.ToInt32(reader["Match_id"]),
                                    MatchedAccountId = Convert.ToInt32(reader["MatchedAcc_id"])
                                };

                                // Check if the Att column is not null
                                if (!reader.IsDBNull(reader.GetOrdinal("Att")))
                                {
                                    byte[] attBytes = (byte[])reader["Att"];
                                    letter.Att = new FormFile(new MemoryStream(attBytes), 0, attBytes.Length, "Att", "attachment");
                                }


                                receivedLetters.Add(letter);
                            }
                        }
                    }

                    return Ok(receivedLetters);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());  // Print the exception details
                return StatusCode(500, ex);
            }
        }

        //Delete Letter
        [HttpDelete("{letterId}")]
        public async Task<IActionResult> DeleteLetter(int letterId)
        {
            string connectionString = _configuration.GetConnectionString("DbmsDatabase");

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    string deleteQuery = "DELETE FROM LETTER WHERE Le_id = @LetterId";

                    using (SqlCommand command = new SqlCommand(deleteQuery, connection))
                    {
                        command.Parameters.AddWithValue("@LetterId", letterId);

                        int rowsAffected = await command.ExecuteNonQueryAsync();

                        if (rowsAffected > 0)
                        {
                            return Ok($"Letter with LetterId {letterId} has been deleted.");
                        }
                        else
                        {
                            return NotFound($"Letter with LetterId {letterId} not found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return StatusCode(500, ex);
            }
        }
    }
}